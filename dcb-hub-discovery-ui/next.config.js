/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  publicRuntimeConfig:{
    KEYCLOAK_ISSUER: process.env.KEYCLOAK_ISSUER,
    DCB_API_BASE: process.env.DCB_API_BASE,
    DCB_ES_URL: process.env.DCB_ES_URL,
    REQUESTER_NEEDS_ROLE: "BETA_TESTER"
  }
}

module.exports = nextConfig
