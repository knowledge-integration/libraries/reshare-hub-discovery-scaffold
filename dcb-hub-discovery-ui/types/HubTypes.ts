export type ResourceDescription = {
    id: string;
    title: string;
};

export type ESAggregation = {
  doc_count_error_upper_bound: number;
  sum_other_doc_count: number;
};

// export type AggregationMap = Map<string, ESAggregation>

export type ESHits = {
  hits: ESSearchResult[];
  total: any;
};

export type ESSearchResponse = {
  hits: ESHits;
  // aggregations: AggregationMap;
  aggregations: Map<string, ESAggregation>;
};

export type ESSearchResult = {
  _index: string;
  _id: string;
  _score: number;
  _source: any;
};

export type MNPageable = {
  size: number;
  sort: string;
  number: number;
};

export type MNPage ={
  content: any;
  pageable: MNPageable;
  totalSize: number;
};

export type PatronRequest = {
  id: string;
  dateCreated: number;
  patronId: string;
  homeLibraryCode: string;
  bibClusterId: string;
  pickupLocationCode: string;
  status: string;
  description: string;
};
