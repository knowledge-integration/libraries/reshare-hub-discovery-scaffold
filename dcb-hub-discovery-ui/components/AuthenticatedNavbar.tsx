import type { NextPage } from 'next'
import Head from 'next/head'
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Button from 'react-bootstrap/Button';
import Link from "next/link";
import { useSession, signIn, signOut } from "next-auth/react"
import { useTranslation, Trans } from 'react-i18next';
import getConfig from 'next/config'

const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();

interface AuthenticatedNavbarProps {
  session: any;
}



const AuthenticatedNavbar = ( {session} : AuthenticatedNavbarProps ) => {

  const { t, i18n } = useTranslation();

  // Per https://www.reddit.com/r/nextjs/comments/redv1r/nextauth_signout_does_not_end_keycloak_session/
  const doLogout = () => {
    // per https://github.com/nextauthjs/next-auth/issues/4612 - await signOut({ callbackUrl: "/api/auth/logout" });
    // See [...next-auth].ts events for what happens after
    // http://{KEYCLOAK_URL}/auth/realms/{REALM_NAME}/protocol/openid-connect/logout?redirect_uri={ENCODED_REDIRECT_URI}
    console.log("doLogout");
    // const host = encodeURIComponent(window.location);
    // const callback_url = publicRuntimeConfig.KEYCLOAK_ISSUER+'/protocol/openid-connect/logout?redirect_uri='+host;
    // console.log("doLogout - calling signOut with callbacl set to %s",callback_url);
    signOut({callbackUrl: '/auth/federatedLogout'});
  }

  /*
  const admin_menu = session.isAdmin == false ? null : (
    <NavDropdown title="Admin" id="basic-nav-dropdown">
      <Link href="/admin/zones" passHref><NavDropdown.Item>Zones</NavDropdown.Item></Link>
      <NavDropdown.Item href="#action/3.2">Admin Another action</NavDropdown.Item>
    </NavDropdown>
    {admin_menu}
  );
  */

  
  // If the config has a REQUESTER_NEEDS_ROLE defined, assert that the user has that role.
  const authorized = publicRuntimeConfig.REQUESTER_NEEDS_ROLE != null ? session?.profile?.roles?.includes(publicRuntimeConfig.REQUESTER_NEEDS_ROLE) : true;

  return (
    <Navbar id="navbarScroll" expand="lg" variant="dark" bg="dark">
      <Container>
        <Nav className="me-auto my-2 my-lg-0"
             style={{ maxHeight: '100px' }}
             navbarScroll >
          <Navbar.Brand href="/">{t('appname')} ({ authorized ? t('authorized') : t('noAuthorization') }) </Navbar.Brand>
          { authorized && <Link href="/patron/requests" className="nav-link" passHref>Requests</Link> }
        </Nav>

        <Nav>
          <NavDropdown title={session.profile.name} id="basic-nav-dropdown">
            <NavDropdown.Item href="/profile">Profile</NavDropdown.Item>
            <NavDropdown.Item onClick={() => doLogout()}>Sign out</NavDropdown.Item>
          </NavDropdown>
        </Nav>
      </Container>
    </Navbar>
  )
}


export default AuthenticatedNavbar;
