import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import styles from '../styles/Home.module.css'
import '../components/i18n';
import React, { useEffect, useState } from "react";
import getConfig from 'next/config'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Link from "next/link";
import {useRouter} from 'next/router'
import Card from 'react-bootstrap/Card';
import { ESSearchResult, ESSearchResponse, ESAggregation } from '../types/HubTypes'
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQuoteLeft, faBook } from '@fortawesome/free-solid-svg-icons';
// https://react-bootstrap.github.io/components/pagination/
import Pagination from 'react-bootstrap/Pagination';

const inter = Inter({ subsets: ['latin'] });

const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();

/**
 * Search widget is the search dialog BUT it can be used from the root of the app in a context free way - I.E. Everything
 * OR /context/ where a set of filters and/or other context specific settings can be applied - for example to limit
 * search results to a specific network, subcluster or library - /ctx/mobius or /ctx/prospector or /cxt/somelib
 */
export default function SearchWidget() {

  const pagesize=10;
  const router = useRouter()
  const [ query, setQuery] = useState("")
  const [ page, setPage] = useState("1")
  const [ results, setResults] = useState<ESSearchResponse|undefined>(undefined)

  const facet_labels: Record<string,string> = {
    "hostLmsFacet": "Host LMS"
  };

  // Not really happy about this - static optimised pages do not get their params on first render... this use effect
  // is triggered once the router is ready and extracts any query from the url and injects it into the state
  useEffect(()=>{

    if(!router.isReady) return;

    console.log("Setting query %s");

    if (router.query.page)
      setPage(router.query.page?.toString())

    if (router.query.q) {
      const qryval:string = router.query.q?.toString();
      setQuery(qryval)
      executeQuery(qryval, parseInt(router.query.page?.toString() ?? "1"));
    }

  }, [router.isReady]);


  useEffect(()=>{
    console.log("update page");
    executeQuery(query, parseInt(page));
  }, [page]);


  const executeQuery = (qry:string, pg:number) => {

    console.log("executeQuery %s page %d",qry,pg);

    if ( qry == null || qry == "" ) return;
    var PAGESIZE=10;

		// Zero out any current hits
		const empty_aggregations = new Map<string, ESAggregation>();
		setResults({hits:{hits:[], total:0}, aggregations:empty_aggregations});

    const query_json = {
      "size":PAGESIZE,
      "from":(pg-1)*PAGESIZE,
      "track_total_hits": true,
      "query":{
        "query_string":{
          "query":qry
        }
      },
      "aggs":{
        "hostLmsFacet":{
          "terms":{
            "field":"members.sourceSystemCode.keyword",
						"size": 20
          }
        }
      }
    };
 
    const fetchData = async () => {
      const url_endpoint = publicRuntimeConfig.DCB_ES_URL+"/_search/";
      const offset = (pg-1)*pagesize;
      console.log("request from %s, query=%s, page=%s",url_endpoint, qry, pg);
      const esresponse = await axios.post(url_endpoint, query_json);
      setResults(esresponse.data);
    };
    fetchData();
  };

  const handleSubmit = (event:any) => {

    console.log("search submit: %s",query);

    event.preventDefault();
    router.push({
      pathname: router.pathname, // used to be '/search',
      query: {q: query},
    })
    executeQuery(query,1);
  }

  const renderSearchResult = (result: ESSearchResult, index: number) => {
    const edition_information = result._source.metadata?.edition != null ? " ("+result._source.metadata.edition+")" : "";
    return (
      <Card key={index} className="text-sm">
        <Card.Body>
          <Card.Title>
            <Link className="nav-link" href={'/resourceDescription/'+result._id}>{result._source.title}{edition_information}</Link>
          </Card.Title>
          <Card.Text>
            <div className="float-end">
              <Link href={{pathname:"/patron/placeRequest",
                           query:{
                             resourceId: result._id
                           }
                      }} passHref><Button className="btn btn-sm">Request this item</Button></Link><br/>
             <FontAwesomeIcon icon={faQuoteLeft} /> &nbsp;
             <FontAwesomeIcon icon={faBook} />
            </div>
            {result._source.metadata == null ? "METADATA IS NULL" : ""}
            {result._source.derivedType} | {result._source.dateOfPublication} <br/>
          </Card.Text>
        </Card.Body>
      </Card>
    )
  }

  const renderFacet = (key:string,value:any) => {
      return ( <>
        <h3>{facet_labels[key]}</h3>
        { value.buckets.map( (v:Record<string,Object>) => <>{v.key}: ({v.doc_count})<br/></> ) }
      </> );
  }

  const renderFacets = (f:any) => {

    let facets = [];

    if ( ! ( f === undefined ) ) {
      for (const [key, value] of Object.entries(f)) {
        facets.push(renderFacet(key,value));
        console.log("%s %o",key,value);
      }
    }
    return facets;
  }

  const handleChange = ( e:any, field:string ) => {
    console.log("updating query %s",e.target.value);
    setQuery(e.target.value);
  }


  let pageno=parseInt(page);
  let items = [];
  let pagination = null;
  let facets = null;
  let records_per_page = 10;
  let num_pages = 0;
  let pageinfo = null;
  const firstrec = ((pageno-1)*pagesize)+1;
  let lastrec = Math.min(firstrec+pagesize,results?.hits?.total?.value);

  const result_count_element = results?.hits?.total?.value != null ? <>Showing hits {firstrec} to {lastrec} of {results?.hits?.total?.value}</> : <></>

  if ( results?.hits?.total?.value > 10 ) {

    num_pages = Math.floor(results?.hits?.total?.value / records_per_page);

    let first_nav_page = Math.max(1, pageno-5);
    let last_nav_page = Math.min(num_pages, pageno+5);

    pageinfo = "Page "+pageno+" of "+num_pages;

    for (let number = first_nav_page; number <= last_nav_page; number++) {
      items.push(
        <Pagination.Item key={number} active={number === pageno} onClick={() => setPage(""+number)} >
          {number}
        </Pagination.Item>,
      );
    }

    let previous_page = pageno > 1 ? <Pagination.Prev onClick={() => setPage(""+(pageno-1))} /> : null;
    let next_page = pageno < num_pages ? <Pagination.Next onClick={() => setPage(""+(pageno+1))} /> : null;

    pagination = ( <Pagination>
         <Pagination.First onClick={() => setPage("1")}/>
         {previous_page}
         {items}
         {next_page}
         <Pagination.Last onClick={() => setPage(""+num_pages)}/>
       </Pagination> )
  }

              // {results?.aggregations?.entries.map(renderFacet)}

  return (
    <>
      <Form onSubmit={handleSubmit}>
        <Form.Group className="mb-3">
          <Form.Label>Search:</Form.Label>
          <Form.Control onChange={e => handleChange(e,'selectedTenantName')}
                        value={query}
                        type="text"
                        name="q"/>
        </Form.Group>
        <Form.Group className="mb-3">
          {result_count_element}
        </Form.Group>
      </Form>
      <Container>
        <Row>
          <Col sm="3">
            <ul>
              {renderFacets(results?.aggregations)} 
            </ul>
          </Col>
          <Col sm="9">
            {results?.hits?.hits?.map(renderSearchResult)}
          </Col>
        </Row>
      </Container>
      
      <Container className="d-flex justify-content-center align-items-center mt-4">
      {pageinfo}
      </Container>
      <Container className="d-flex justify-content-center align-items-center mt-4">
      {pagination}
      </Container>
    </>
  )
}
