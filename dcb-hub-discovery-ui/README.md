This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Prereqs

It might be helpful to use nvm to manage your node version locally

    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
    nvm install node
    nvm use node
    npx install --global yarn

## Getting Started

First, run the development server:

Checkout the project and then create a .env.local file that points to the development keycloak contents should be


    NEXTAUTH_SECRET=MAKE_THIS_UP_HOWEVER_YOU_LIKE
    KEYCLOAK_SECRET=GET_THIS_FROM_THE_DEV_TEAM
    KEYCLOAK_ISSUER=keycloak-realm-url
    KEYCLOAK_ID=dcbclient
    NEXTAUTH_URL=http://localhost:3000
    DCB_API_BASE=https://dcb.libsdev.k-int.com/


Firstly, run a..
```bash
npm install
```
if you havent already. Then..
```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.tsx`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.ts`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.




