import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import styles from '../../styles/Home.module.css'
import '../../components/i18n';
import React, { useEffect, useState } from "react";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Spinner from 'react-bootstrap/Spinner';
import { Table } from 'react-bootstrap'
import Link from "next/link";
import { useRouter } from 'next/router'
import { useSession, signIn, signOut } from "next-auth/react"
import Modal from 'react-bootstrap/Modal';
import axios from 'axios';
import { ResourceDescription } from '../../types/HubTypes';
import getConfig from 'next/config'

const inter = Inter({ subsets: ['latin'] });

const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();

export interface PatronRequestModel {
  instanceClusterId: string;
  patronId: string;
  homeLocation: string;
  pickupLocation: string;
  localSystemCode: string;
}

const ResourceDescriptionPage = () => {

  // This funky syntax is the typescript typing when destructuring.. not sure it's any better than the long version
  const { data: session, status } : {data:any, status:any}  = useSession()

  const router = useRouter()
  const [ resourceId, setResourceId] = useState("")
  const [ resourceDescription, setResourceDescription] = useState<any>({});
  const [ items, setItems] = useState<any>({fetched:false, items:[]});
  const [ show, setShow] = useState(false);
  const [ patronRequest, setPatronRequest] = useState<any>({localSystemCode:session?.profile?.localSystemCode, patronId:session?.profile?.preferred_username});

  console.log("Session: %o",session);

  useEffect(()=>{
    if(!router.isReady) return;
    if (router.query.resourceId) {
      setResourceId(router.query.resourceId?.toString())
    }
  }, [router.isReady]);

  useEffect(()=>{
    if ( ( resourceId != null) && ( resourceId.length > 0 ) ) {

      setPatronRequest((prev:any) => ({...prev, ['instanceClusterId']: resourceId}) );

      const fetchData = async () => {
        const url_endpoint = publicRuntimeConfig.DCB_ES_URL+"/_doc/"+router.query.resourceId;
        console.log("request from %s",url_endpoint);
        const esresponse = await axios(url_endpoint, {} );
        console.log("Got data %o",esresponse);
        // Worried that once the ES object has finished, it throws away the data - so clone the record.
        setResourceDescription(structuredClone(esresponse.data?._source));
      };
      fetchData();
    }
  }, [resourceId]);

  // https://flowbite.com/docs/components/spinner/

  useEffect(()=>{
    console.log("rd: %o",resourceDescription);
    if ( ( resourceDescription != null ) && ( resourceDescription.bibClusterId != null ) ) {
      const live_availability_url_endpoint = publicRuntimeConfig.DCB_API_BASE+"/items/availability";
      // const live_availability_url_endpoint = "https://dcb.libsdev.k-int.com/items/availability?wibble="+resourceDescription.sourceRecordId;

      // Without the use of StructuredClone above ^^ the data in this record may have vanished by the time we try and retrive the holdings.
      // const query_params = { bibRecordId: resourceDescription.sourceRecordId, hostLmsCode: resourceDescription.sourceSystemCode };
      const query_params = { clusteredBibId: resourceDescription.bibClusterId };

      console.log("Try to find availability for %o",query_params);
      axios.get(live_availability_url_endpoint, {  
        params: query_params,
        // headers: { "Access-Control-Allow-Origin":"*" }
      } ).then(response => {
        console.log("itemList: %o", response.data);
        setItems({fetched: true, items: response.data.itemList});
      } );
    }
    else {
      console.log("No bibs");
      // setItems({fetched:true, items:[]});
    }
  }, [resourceDescription]);

  const renderItem = (result: any, index: number) => {
    return (
      <tr key={result.id}>
        <td>{result.id}</td>
        <td>{result.status?.code}</td>
        <td>{result.status?.displayText}</td>
        <td>{result.hostLmsCode}</td>
        <td>{result.agency?.description}({result.agency?.code})</td>
        <td>{result.location?.code}</td>
        <td>{result.location?.name}</td>
        <td>{result.barcode}</td>
        <td>{result.callNumber}</td>
        <td>{result.isRequestable==true?"Yes":"No"}</td>
        <td>{result.holdCount}</td>
        <td>{result.canonicalItemType}</td>
        <td>{result.localItemTypeCode}</td>
        <td>{result.rawVolumeStatement}</td>
        <td>{result.parsedVolumeStatement}</td>
      </tr>
    )
  }

  // Curry a prefix for the key
  function createLabelRenderer(prefix: String) {
    /* eslint-disable react/display-name */
    return function(result: any, index: number) {
      const keystr = `${prefix}-${index}`;
      return (<span key={keystr}>{result.label}</span>)
    };
  };

  // Returns 'true' if at least one status code equals "Available"
  const canRequest = items?.items?.some((item: { status: { code: string; }; }) => item?.status?.code === 'AVAILABLE')

  // Authors: <strong>{resourceDescription?.metadata?.agents?.map(createLabelRenderer("agent"))}</strong><br/>
  return (
    <>
      <main className={styles.main}>
        <Container>
          <Row>
            <Col sm={12}>
              <Link href={{pathname:"/patron/placeRequest",
                           query:{
                             resourceId: router.query.resourceId
                           }
                    }} passHref><Button disabled={!canRequest} className="float-end">Request this item</Button></Link>
              Bib Record Id: {router.query.resourceId}<br/>
              DCB Cluster Id: {resourceDescription?.bibClusterId}<br/>
              Title: <strong>{resourceDescription?.title}</strong><br/>
              Subjects: <strong>{resourceDescription?.metadata?.subjects?.map(createLabelRenderer("subject"))}</strong><br/>
              <em>{resourceDescription?.derivedType}</em><br/>
              <div>
                <Table responsive bordered hover>
                  <thead className="bg-light">
                    <tr>
                      <th>#</th>
                      <th>Status Code</th>
                      <th>Status Text</th>
                      <th>Host System</th>
                      <th>Agency</th>
                      <th>Location Code</th>
                      <th>Location Name</th>
                      <th>Copy Barcode</th>
                      <th>Call Number</th>
                      <th>Requestable?</th>
                      <th>Hold count</th>
                      <th>IType (Canon)</th>
                      <th>IType (Local)</th>
                      <th>Raw Vol</th>
                      <th>Parsed Vol</th>
                    </tr>
                  </thead>
                  <tbody>
                    {items?.items?.map(renderItem)}
                  </tbody>
                </Table>
                { items.fetched === false && <Spinner animation="border" role="status"> <span className="visually-hidden">Loading...</span> </Spinner> }
              </div>
            </Col>
          </Row>
          <Row>
            <pre>
              {JSON.stringify(resourceDescription,null,2)}
            </pre>
          </Row>
        </Container>
      </main>  
    </>
  )
}

export default ResourceDescriptionPage;

