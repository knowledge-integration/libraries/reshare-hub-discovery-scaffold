import '../styles/globals.css'
import type { AppProps } from 'next/app'
import Head from 'next/head'
import { SessionProvider } from "next-auth/react"
import Layout from '../components/Layout'

import { config } from '@fortawesome/fontawesome-svg-core'
import '@fortawesome/fontawesome-svg-core/styles.css'

// You change this configuration value to false so that the Font Awesome core SVG library
// will not try and insert <style> elements into the <head> of the page.
// Next.js blocks this from happening anyway so you might as well not even try.
// See https://fontawesome.com/v6/docs/web/use-with/react/use-with#next-js
config.autoAddCss = false


export default function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <title>DCB Hub Discovery Scaffold</title>
      </Head>
      <SessionProvider session={pageProps.session}>
        <Layout>
          <Component className="fluid" {...pageProps} />
        </Layout> 
      </SessionProvider>
    </>
  )
}

