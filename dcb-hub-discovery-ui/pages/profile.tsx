import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import styles from '../styles/Home.module.css'
import '../components/i18n';
import React, { useEffect, useState } from "react";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Link from "next/link";
import {useRouter} from 'next/router'
import SearchWidget from '../components/SearchWidget'
import { useSession, signIn, signOut } from "next-auth/react"
import type { NextPage } from 'next'

const inter = Inter({ subsets: ['latin'] });

const ProfilePage: NextPage = () => {

  const { data: session, status } : {data:any, status:any} =useSession();

  return (
    <main className={styles.main}>
      <Container>
        <Row>
          <Col sm={12}>
            <pre>
              {JSON.stringify(session,null,2)}
            </pre>
          </Col>
        </Row>
      </Container>
    </main>
  )
}

export default ProfilePage;
