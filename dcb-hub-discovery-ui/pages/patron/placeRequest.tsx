import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import styles from '../../styles/Home.module.css'
import '../../components/i18n';
import React, { useEffect, useState } from "react";
import { useSession, signIn, signOut } from "next-auth/react"
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Link from "next/link";
import {useRouter} from 'next/router'
import axios from 'axios';
import getConfig from 'next/config'

const inter = Inter({ subsets: ['latin'] });
const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();

export default function PlaceRequestPage() {

  // This funky syntax is the typescript typing when destructuring.. not sure it's any better than the long version
  const { data: session, status } : {data:any, status:any}  = useSession()

  const router = useRouter()
  const [ resourceId, setResourceId] = useState("")
  const [ resourceDescription, setResourceDescription] = useState<any>({});
  const [ locations, setLocations] = useState([]);
  const [ buttonDisabled, setButtonDisabled] = useState(true);
  const [ patronRequest, setPatronRequest] = useState<any>({});
  const [ dcbResponse, setDcbResponse] = useState<any>({});

	const locations_graphql ={ 
		"query": "query($lq: String) { locations(query: $lq, pagesize:1000, order:\"name\") { totalSize, pageable { number, offset }, content { id, code, name, type, isPickup, agency { name } } } }",
		"variables": {
			"lq" : "type:PICKUP"
		}
	};

  useEffect( () => {
    if ( status == 'authenticated' ) {
      setPatronRequest((prev:any) => ({...prev, ['localSystemCode']: session?.profile?.localSystemCode}));
      // setPatronRequest((prev:any) => ({...prev, ['patronId']: session?.profile?.preferred_username}));
      setPatronRequest((prev:any) => ({...prev, ['patronId']: session?.profile?.localSystemPatronId}));
      setPatronRequest((prev:any) => ({...prev, ['homeLibraryCode']: session?.profile?.homeLibraryCode}));
    }
  }, [status]);

  useEffect(()=>{
    if(!router.isReady) return;
    if (router.query.resourceId) {
      setResourceId(router.query.resourceId?.toString())
    }
  }, [router.isReady]);

  useEffect(()=>{
    if ( ( resourceId != null) && ( resourceId.length > 0 ) && ( session != null ) ) {

      setPatronRequest((prev:any) => ({...prev, ['instanceClusterId']: resourceId}) );

      const fetchData = async () => {
        const url_endpoint = publicRuntimeConfig.DCB_ES_URL+"/_doc/"+router.query.resourceId;
        console.log("request from %s",url_endpoint);
        const esresponse = await axios(url_endpoint, {} );
        console.log("Got data %o",esresponse);
        setResourceDescription(esresponse.data?._source);

				const jwt = session?.accessToken;

        // Fetch locations from https://dcb.libsdev.k-int.com/locations
        // const locations_url_endpoint = "https://dcb.libsdev.k-int.com/locations"
        const graphql_endpoint = publicRuntimeConfig.DCB_API_BASE+"/graphql"
        const locations_response = await axios.post(graphql_endpoint, locations_graphql, { headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` } } );
        console.log("Got locations %o",locations_response);
				setLocations(locations_response.data.data.locations.content);
        // const filtered_locations = locations_response.data.content.filter( (loc:any) => ( ( loc.type=="PICKUP" ) && ( loc.name != null ) ) )
        // setLocations(filtered_locations.sort((a:any,b:any) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0)) );
      };
      fetchData();
    }
  }, [resourceId, session]);

  const renderLocation = (location: any, index: number) => {
    return (
      <option key={index} value={location.id}>{location.name} ({location.code} @ {location.agency?.name})</option>
    )
  }

  const handleChange = ( e:any, field:string ) => {
    setButtonDisabled(false);
    setPatronRequest((prev:any) => ({...prev, [field]: e.target.value}));
  }

  const placeRequest = () => {

		// const url_endpoint = "https://dcb.libsdev.k-int.com/patrons/requests/place";
		const url_endpoint = publicRuntimeConfig.DCB_API_BASE+"/patrons/requests/place";
		const jwt = session.accessToken;

		console.log("request post %o %o to %s",resourceDescription, patronRequest, url_endpoint);
		const patron_request_payload = {
      citation : {
        bibClusterId: resourceDescription.bibClusterId
      },
      requestor: {
        localSystemCode: patronRequest.localSystemCode,
        localId: patronRequest.patronId,
        homeLibraryCode: patronRequest.homeLibraryCode
      },
      pickupLocation: {
        code: patronRequest.pickupLocation
      },
      "description": resourceDescription.title
		};

		axios.post(url_endpoint, patron_request_payload, { headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` } } )
		.then (  ( response ) => {
			console.log("Got data %o",response);
			setDcbResponse( { status: "PLACED", responseObject: response });
		})
		.catch ( (error) => {
			console.log("Error: %o",error);
			setDcbResponse( { status: "ERROR", responseObject: error.response.data });
		});
  }

  //const authorized = publicRuntimeConfig.REQUESTER_NEEDS_ROLE != null ? session?.profile?.roles?.includes(publicRuntimeConfig.REQUESTER_NEEDS_ROLE) : true;
  const authorized = true;


  const place_request_button = authorized ? ( <>
    <Button variant="primary" onClick={placeRequest} disabled={buttonDisabled}>
      Place Request
    </Button>
  </> ) : ( <p>Please request BETA access to submit requests</p> );

  const thanks_message = ( <>
    <p>Thankyou for your request</p>
    <p>Your item will be available at your pick up location in 2 to 3 days</p>
    <p>We have sent you a notificaion in accordance with your preferences</p>
    <p>You will be notified when your item is available for collection</p>
    <pre>{JSON.stringify(dcbResponse,null,2)}</pre>
  </> );

  const error_message = ( <>
		<p>ERROR</p>
		<p>Unable to place request</p>
    <pre>{JSON.stringify(dcbResponse,null,2)}</pre>
	</> );

  const renderResponse = () => {
		switch ( dcbResponse?.status ) {
      case 'PLACED':
        return thanks_message;
        break;
      case 'ERROR':
        return error_message;
        break;
			default:
				return place_request_button;
        break;
    }
	}

  return (
    <main className={styles.main}>
      <Container>
        <Row>
          <Col sm={12}>
            Session Status: {status}
            <Form.Group className="mb-3">
              <Form.Label htmlFor="requestTitle">Request a copy of</Form.Label>
              <Form.Control id="requestTitle" placeholder={resourceDescription?.title} disabled />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="patronBarcode">For</Form.Label>
              <Form.Control id="patronBarcode" placeholder={patronRequest?.patronId} disabled />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>To be picked up from</Form.Label>
              <Form.Select value={patronRequest.pickupLocation} onChange={e => handleChange(e,'pickupLocation')}>
                {locations?.map(renderLocation)}
              </Form.Select>
            </Form.Group>
            {renderResponse()}
          </Col>
        </Row>
      </Container>
    </main>
  )
}
