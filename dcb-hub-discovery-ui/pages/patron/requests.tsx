import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import styles from '../../styles/Home.module.css'
import '../../components/i18n';
import React, { useEffect, useState } from "react";
import { useSession, signIn, signOut } from "next-auth/react"
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Link from "next/link";
import {useRouter} from 'next/router'
import axios from 'axios';
import { MNPage, PatronRequest } from '../../types/HubTypes'
import Card from 'react-bootstrap/Card';
import getConfig from 'next/config'

const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();
const inter = Inter({ subsets: ['latin'] });

export default function BookshelfPage() {

  // This funky syntax is the typescript typing when destructuring.. not sure it's any better than the long version
  const { data: session, status } : {data:any, status:any}  = useSession()
  const [ patronRequests, setPatronRequests] = useState<MNPage|undefined>(undefined)

  // https://dcb.libsdev.k-int.com/patrons/requests
  useEffect(()=>{
     const fetchData = async () => {
        const url_endpoint = publicRuntimeConfig.DCB_API_BASE+"/patrons/requests";
        const jwt = session.accessToken;
        const prs = await axios(url_endpoint, 
                                {  params : {},
                                   headers: {
                                         Authorization: `Bearer ${jwt}`,
                                   } } );
        // console.log("Got data %o",esresponse);
        setPatronRequests(prs.data);
     };
     fetchData();
  }, [session] );


  const router = useRouter()

  // Patron Id: {pr.patronId} <br/>
  // Patron Home: {pr.homeLibraryCode} <br/>
  const renderPatronRequest = (pr: PatronRequest, index: number) => {
    return (
      <Card key={index}>
        <Card.Body>
          <Card.Title><Link className="nav-link" href={'/patron/requests/'+pr.id}>{pr.description}</Link></Card.Title>
          <Card.Text>
            Date placed: {new Date(pr.dateCreated).toString()} <br/>
            Cluster Record Id: {pr.bibClusterId} <br/>
            Pickup Location: {pr.pickupLocationCode} <br/>
            Request Status: {pr.status}
          </Card.Text>
        </Card.Body>
      </Card>
    )
  }

  return (
    <main className={styles.main}>
      <Container>
        <Row>
          <Col sm={12}>
            <h1>Your Requests</h1>
            {patronRequests?.content?.map(renderPatronRequest)}
          </Col>
        </Row>
      </Container>
    </main>
  )
}
